package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Login
 */
@WebServlet("/login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=UTF-8");
	//	response.getWriter().append("Served at: ").append(request.getContextPath());
		PrintWriter out=response.getWriter();
		out.print("<br>");
		out.print("Haciendo login...");
		out.print("<br>");
		String nombre=request.getParameter("name");
		out.print("Usuario:"+nombre+"<br>");
		out.print("<br>");
		
		String contraseña=request.getParameter("password");
		out.print("contraseña:"+contraseña+"<br>");
		out.print("<br>");
		
		String nuevo=request.getParameter("nuevo");
		out.print("Nuevo?:"+nuevo+"<br>");
		
		String [] aficiones=request.getParameterValues("aficiones");
		
		
		out.println("<h4>Aficiones</h4>");
		out.println("<ul>");
		for(String aficion: aficiones){
			 System.out.println(aficion);
			 out.print("<ul>"+aficion+"</ul>");
		}
		out.println("</ul>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
